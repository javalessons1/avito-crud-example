package org.example;

import org.example.manager.AvitoManager;
import lombok.extern.slf4j.Slf4j;
import org.example.data.Apartment;
import org.example.dto.Request;

import java.util.ArrayList;

@Slf4j
public class Main {
    public static void main(String[] args) {
        String [] requestedRoomAmount1 = {"Studio"};
        String [] requestedRoomAmount3 = {"Studio", "freePlanned", "1", "2", "3", "4", "fiveAndMore"};
        Apartment apartmentStudio = new Apartment(0, "Studio", 4_000_000, 55, true, true, 4, 5);
        Apartment apartmentOneRoom= new Apartment(1, "1", 1_000_000,  35, false, true, 9, 9);
        Apartment apartmentFirstFloor = new Apartment(2, "freePlanned", 1_000_000, 72, false, false, 1, 15);

        Request requestSearchOne = new Request(requestedRoomAmount1, 3_000_000, 5_000_000, 30, null, true, false,
                null, null, null, null, false, false);
        Request requestSearchAll = new Request(requestedRoomAmount3, null, null, null, null, false, false,
                null, null, null, null, false, false);

        AvitoManager manager = new AvitoManager();

        manager.create(apartmentStudio);
        manager.create(apartmentOneRoom);
        manager.create(apartmentFirstFloor);
        log.debug("Was created {} items.", manager.getAll().size());

        ArrayList<Apartment> items1 = manager.search(requestSearchOne);
        log.debug("Was find {} items : {}", items1.size(), items1);

        apartmentOneRoom.setPrice(2_000_000);
        manager.update(apartmentStudio);
        log.debug("Current price: {}", apartmentOneRoom.getPrice());

        manager.removeById(0);
        log.debug("Was removed 1 item. Current amount: {} items.", manager.getAll().size());

        ArrayList<Apartment> items2 = manager.search(requestSearchAll);
        log.info("All apartment available for buy: {}.", items2);
    }
}
